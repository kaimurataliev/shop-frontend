import React, { Component, Fragment } from 'react';
import Products from './containers/Products/Products';
import Toolbar from "./components/UI/Toolbar/Toolbar";

import {Route, Switch} from 'react-router-dom';
import NewProduct from "./containers/NewProduct/NewProduct";

class App extends Component {
  render() {
    return (
    <Fragment>
        <header><Toolbar/></header>
        <div className="container">
            <main>
                <Switch>
                    <Route path="/" exact component={Products}/>
                    <Route path="/product/new" exatc component={NewProduct}/>
                </Switch>
            </main>
        </div>
    </Fragment>
    );
  }
}

export default App;
