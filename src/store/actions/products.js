import {FETCH_PRODUCTS_SUCCESS, CREATE_PRODUCTS_SUCCESS} from "./actionsTypes";
import axios from '../../axiox-api';

export const fetchProductsSuccess = (products) => {
    return {type: FETCH_PRODUCTS_SUCCESS, products}
};

export const fetchProducts = () => {
    return (dispatch) => {
        axios.get('/products').then(
            response => {
                dispatch(fetchProductsSuccess(response.data))
            }
        )
    }
};

export const createProductsSuccess = () => {
    return {type: CREATE_PRODUCTS_SUCCESS}
};

export const createProduct = (productData) => {
    return (dispatch) => {
        axios.post('/products', productData).then(
            response => {
                dispatch(createProductsSuccess());
            }
        )
    }
};