import React, { Component, Fragment } from 'react';
import {PageHeader, Button, Panel} from "react-bootstrap";
import {connect} from 'react-redux';
import {fetchProducts} from "../../store/actions/products";
import { Link } from "react-router-dom";

class Products extends Component {

    componentDidMount() {
        this.props.onFetchProducts();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Products
                    <Link to="/product/new"><Button bsStyle="primary" className="pull-right">Add product</Button></Link>
                </PageHeader>

                {this.props.products.map((product) => {
                    return (
                        <Panel key={product.id}>
                            <Panel.Body>
                                <Link to={"/products" + product.id}>
                                    {product.title}
                                </Link>
                                <strong style={{marginLeft: '10px'}}>
                                    {product.price}
                                </strong>
                            </Panel.Body>
                        </Panel>
                    )
                })}
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.products.products
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProducts: ()=> dispatch(fetchProducts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);