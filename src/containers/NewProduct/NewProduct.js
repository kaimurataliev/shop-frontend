import React, {Component, Fragment} from 'react';
import ProductForm from "../../components/ProductForm/ProductForm";

class NewProduct extends Component {
    render() {
        return (
            <Fragment>
                <h2>New product</h2>
                <ProductForm />
            </Fragment>
        );
    }
}

export default NewProduct;
